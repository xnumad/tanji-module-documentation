# Tanji Module Documentation

# Set Up New Module[^Mika'sTutorialPart1] [^DarkboxUrbanResource] [^XePeleato'sTutorial]

- Create Visual Studio Project "C# Windows Forms App (.NET Framework)" targeting `.NET Framework 4.8`
- Add `Sulakore`, `Tangine` and `Flazzy` as References (either as Projects[^PublikFurniProjectDependenciesCommit] or as DLLs)
- Add these using directives to the beginning of `Form1.cs` to access their namespaces:

```csharp
using Sulakore.Communication;
using Sulakore.Components;
using Sulakore.Habbo;
using Sulakore.Modules;
using Sulakore.Protocol;

using Tangine;
```

- In `public partial class Form1 : Form` replace `Form` with `ExtensionForm`
- Before that line, add the line `[Module("<Module name>",  "<Module description>")]`
- (Optional) add the line `[Author("Your name")]`

# Get Headers (Packet IDs) From Message Hashes

## Indirectly read from `Hashes.ini` dictionary using Sulakore[^InOutDefinition] [^SulakoreHabboMessages]:

| Message Direction | Property
|--|--
| Outgoing | `Out.<HashName>`
| Incoming | `In.<HashName>`

## Alternatively, provide the message hash yourself:

### Using GetMessageIds method

```csharp
Game.GetMessageIds(<hash>).FirstOrDefault()
```

### Using MessageId attribute
```csharp
[MessageId(<hash>)]
public ushort myMessageId { get; set; }
```

# Send Packet[^Mika'sTutorialPart2]

> In order to do that we'll make use of
> `Connection`, inherited from `ExtensionForm`,
> that allows us to send packets to either the client or the server.[^XePeleato'sTutorial]

| Packet Direction | Method
|--|--
| Outgoing | `Connection.SendToServerAsync()`
| Incoming | `Connection.SendToClientAsync()`

Inside the parentheses, you write the packet structure.

| type | how to write | example structure | example code
|--|--|--|--
| string | in quotation marks | `{s:Hello}` | `"Hello"`
| integer | just write the integer number | `{i:1337}` | `1337`
| boolean | write `true` or `false`, without quotation marks | `{b:true}` | `true`

Example:
- Packet structure: `{l}{u:2590}{i:1337}{s:Hello}{s:0}{s:}{b:true}{b:false}`
- **Easiest code** `Connection.SendToServerAsync(2590, 1337, "Hello", "0", "", true, false)`

## Save Packet in HMessage Before Sending

```csharp
HMessage somePacket = new HMessage(2590, 1337, "Hello", "0", "", true, false);
//or
HMessage somePacket = new HMessage("{l}{u:2590}{i:1337}{s:Hello}{s:0}{s:}{b:true}{b:false}") //Inconvenient with variables

Connection.SendToServerAsync(somePacket)
```

# Intercept Packet

## Set Up Interception Methods

Either or one of the following

### Triggers.{In|Out}Attach

```csharp
private string chatMessage;

public Form1()
{
    InitializeComponent();
    Triggers.OutAttach(Out.Chat, ParseChatMessage);
}

private void ParseChatMessage(DataInterceptedEventArgs obj)
{
    chatMessage = obj.Packet.ReadString();
}
```

### {In|Out}DataCapture Attribute[^XePeleato'sTutorial] [^BetterChatMainFrm]

```csharp
[OutDataCapture("Chat")] //either the name from Hashes.ini or the hash itself
private void ParseChatMessage(DataInterceptedEventArgs obj)
{
    chatMessage = obj.Packet.ReadString();
}
```

## Read Packet Data

`DataInterceptedEventArgs.Packet.ReadInteger()`

## Replace Packet Data

`DataInterceptedEventArgs.Packet.ReplaceInteger(<replacementInt>)`

where `replacementInt` is the number you want to set the int to.

## Block Packet

`DataInterceptedEventArgs.IsBlocked = true`

# Logging/Forwarding/Passthrough of Packets

## Do _packets sent by a module_ get displayed in the packet logger?

No, because the packet is neither from the server nor the client, but from Tanji (module).

## Do _packets blocked using Tanji Filter settings_ still get intercepted by a module?

Yes, packets still reach a module and are only being blocked by Tanji from reaching their original recipient (the client or the server).

The property `DataInterceptedEventArgs.IsBlocked` is `true` then.

# Footnotes/References/Sources

[^Mika'sTutorialPart1]: [Mika's Tutorial "Visual Studio: Tanji Extension [part 1]"](https://www.youtube.com/watch?v=Gi7cMQesfpU)
[^Mika'sTutorialPart2]: [Mika's Tutorial "Visual Studio: Tanji Extension [part 2]"](https://www.youtube.com/watch?v=j2omxeApj7E)
[^InOutDefinition]: https://github.com/ArachisH/Tanji/blob/master/Tangine/ExtensionForm.cs#L55
[^SulakoreHabboMessages]: https://github.com/ArachisH/Sulakore/tree/master/Sulakore/Habbo/Messages
[^DarkboxUrbanResource]: [Darkbox Urban resource "Tanji/For developers/Plugin development/Setting up"](https://urban.darkbox.nl/tanji/develop/7)
[^XePeleato'sTutorial]: [XePeleato's SnG Forum Thread: "[TUT] How to create a Tanji module"](https://www.sngforum.info/showthread.php?tid=6938)
[^BetterChatMainFrm]: https://github.com/put/BetterChat/blob/master/BetterChat/MainFrm.cs#L54
[^PublikFurniProjectDependenciesCommit]: https://github.com/xnumad/PublikFurni/commit/1f28670
